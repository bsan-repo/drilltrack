#include "StateRepresentationWidget.h"
#include "ui_StateRepresentationWidget.h"

#include <QDebug>
#include <QGraphicsItem>



StateRepresentationWidget::StateRepresentationWidget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::StateRepresentationWidget)
{
    ui->setupUi(this);

    scene_ = new QGraphicsScene(this);
    scene_->setSceneRect(0,0,300,300);

    drillTruckItem_ = NULL;
    drillMastItem_ = NULL;

    stateLabel_ = "STATE UNSET";
    mastAngle_ = 0.0;

    initializeGraphicsView();
}

StateRepresentationWidget::~StateRepresentationWidget()
{
    delete ui;
}

void StateRepresentationWidget::initializeGraphicsView()
{
    qDebug() << "initializeGraphicsView";

    QString drillTruckImagePath = "://images/images/drillTruck.svg";
    //drillTruckItem_ = new DrillGraphicsItem(drillTruckImagePath);
    drillTruckItem_ = new DrillGraphicsItem;
    scene_->addItem(drillTruckItem_);

//    QString drillMastImagePath = "://images/images/drillMast.svg";
//    //drillMastItem_ = new DrillGraphicsItem(drillMastImagePath);
//    drillMastItem_ = new DrillGraphicsItem(drillMastImagePath);
//    drillMastItem_->setPos(200,40);
//    drillMastItem_->setRotation(-180);
//    scene_->addItem(drillMastItem_);

    ui->graphicsView->setScene(scene_);

    qDebug() << "graphics view initialized";
}

void StateRepresentationWidget::setDrillState(e_DrillState drillState)
{
    switch(drillState)
    {
    case DRILL_STATE_IDLE:
        stateLabel_ = "IDLE";
        mastAngle_ = -180;
        break;

    case DRILL_STATE_MOVING:
        stateLabel_ = "MOVING";
        mastAngle_ = -180;
        break;

    case DRILL_STATE_ACTIVE:
        stateLabel_ = "ACTIVE";
        mastAngle_ = -90;
        break;
    }

    drillTruckItem_->setDrillState(drillState);
    ui->stateLabel->setText(stateLabel_);
}

void StateRepresentationWidget::setStateLabel(QString newStateLabel)
{
    stateLabel_ = newStateLabel;
    ui->stateLabel->setText(stateLabel_);
}

void StateRepresentationWidget::refreshGraphicsView()
{
    //drillMastItem_->setRotation(mastAngle_);

    ui->graphicsView->setScene(scene_);
}
