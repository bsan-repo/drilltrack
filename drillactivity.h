#ifndef DRILLACTIVITY_H
#define DRILLACTIVITY_H
#include <QDateTime>

class DrillActivity
{
public:
    int id;
    double lat;
    double lon;
    int statusDrill;
    QDateTime timeStamp;
    int drillId;
    QDateTime createdAt;
    QDateTime updatedAt;
};

#endif // DRILLACTIVITY_H
