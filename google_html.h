#ifndef GOOGLE_HTML_H
#define GOOGLE_HTML_H

#include <iostream>
#include <fstream>
#include <sstream>
#include <list>
#include <string>
#include <vector>

class google_html {
    public:

    // STORAGE
    std::stringstream html_file;
    std::string filename;

    std::vector<std::string> colours;
    std::vector<std::string> colours_hex;

    google_html() {
        colours.push_back("black");
        colours.push_back("red");
        colours.push_back("orange");
        colours.push_back("yellow");
        colours.push_back("green");
        colours.push_back("blue");
        colours.push_back("purple");

        colours_hex.push_back("#FFFFFF");
        colours_hex.push_back("#FF0000");
        colours_hex.push_back("#FF8000");
        colours_hex.push_back("#FFFF00");
        colours_hex.push_back("#00FF00");
        colours_hex.push_back("#0000FF");
        colours_hex.push_back("#FF00FF");
    }

    void generate_begin(int zoom = 10, double centreLat = -22.897954, double centreLon = 144.221384) {

            html_file << "<!DOCTYPE html><html><head><script src='http://maps.googleapis.com/maps/api/js'></script>"
            << "<script>"
            << "    var myCenter = new google.maps.LatLng(" << centreLat << ", " << centreLon << ");"
            << "function initialize() {"
            << "    var mapProp = {"
            << "        center:myCenter,"
            << "        zoom:" << zoom << ","
            << "        mapTypeId:google.maps.MapTypeId.HYBRID"
            << "        };"
            << "        var map=new google.maps.Map(document.getElementById('googleMap'),mapProp);";
    }

    std::string generate_end() {
            html_file << "}"
            << "google.maps.event.addDomListener(window, 'load', initialize);</script></head>"
            << "<body><div id='googleMap' ></div></body>"
            << "</html>"
            << "<style>"
            << "html, body, #googleMap { width: 100%; height: 100%; margin: 0; padding: 0;}"
            << "#map_canvas {position: relative;}"
            << "</style>";

            return html_file.str();
    }

    void generate_rig(std::string name, std::string title, double lat, double lon, int colour = 0) {
                html_file << "        var marker2=new google.maps.Marker({"
                << "            icon: 'http://maps.google.com/mapfiles/kml/pal3/icon" << colour +7
                << ".png',"
                << "            title: '" << title << "',"
                << "            position: new google.maps.LatLng(" << lat << "," << lon << "),});"
                << "        marker2.setMap(map);";
    }

    void generate_marker(std::string name, std::string title, double lat, double lon, int colour = 0) {
                html_file << "        var marker2=new google.maps.Marker({"
                << "            icon: 'http://labs.google.com/ridefinder/images/mm_20_" << colours[colour]
                << ".png',"
                << "            title: '" << title << "',"
                << "            position: new google.maps.LatLng(" << lat << "," << lon << "),});"
                << "        marker2.setMap(map);";
    }

    void generate_polygon(std::string name, std::list<double> lat, std::list<double> lon,
            int colour = 1,
            int weight = 2)
    {
            html_file << "var " << name << "Coordinates = [";

                std::list<double>::iterator lat_it = lat.begin();
                std::list<double>::iterator lon_it = lon.begin();
                for (int i=0 ; i<lat.size() ; i++) {
                    html_file << "new google.maps.LatLng(" << *lat_it << "," << *lon_it << "),";
                    lat_it++; lon_it++;
                }

             html_file << "];"
            << "var " << name << " = new google.maps.Polygon({"
            << "          path: " << name << "Coordinates,"
            << "              geodesic: true,"
            << "                  strokeColor: '" << colour << "',"
            << "                      strokeOpacity: 1.0,"
            << "                          strokeWeight: " << weight << ","
            << "                          fillColor: '" << colours_hex[colour] << "',"
            << "                          fillOpacity: 0.3"
            << "                            });"
            << name << ".setMap(map);";

    }

    void generate_polyline(std::string name, std::list<double> lat, std::list<double> lon,
            int colour = 1,
            int weight = 2)
    {
            html_file << "var " << name << "Coordinates = [";

                std::list<double>::iterator lat_it = lat.begin();
                std::list<double>::iterator lon_it = lon.begin();
                for (int i=0 ; i<lat.size() ; i++) {
                    html_file << "new google.maps.LatLng(" << *lat_it << "," << *lon_it << "),";
                    lat_it++; lon_it++;
                }

             html_file << "];"
            << "var "<< name << " = new google.maps.Polyline({"
            << "          path: " << name << "Coordinates,"
            << "              geodesic: true,"
            << "                  strokeColor: '" << colours_hex[colour] << "',"
            << "                      strokeOpacity: 1.0,"
            << "                          strokeWeight: " << weight
            << "                            });"
            << name << ".setMap(map);";

    }
};

#endif // GOOGLE_HTML_H
