#include "LogNoteWidget.h"
#include "ui_LogNoteWidget.h"

LogNoteWidget::LogNoteWidget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::LogNoteWidget)
{
    ui->setupUi(this);
}

LogNoteWidget::~LogNoteWidget()
{
    delete ui;
}
