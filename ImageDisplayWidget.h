#ifndef IMAGEDISPLAYWIDGET_H
#define IMAGEDISPLAYWIDGET_H

#include <QWidget>

#include <QGraphicsPixmapItem>

namespace Ui {
class ImageDisplayWidget;
}

class ImageDisplayWidget : public QWidget
{
    Q_OBJECT

public:
    explicit ImageDisplayWidget(QWidget *parent = 0);
    ~ImageDisplayWidget();

    void displayImage(QString imagePath);

private:
    Ui::ImageDisplayWidget *ui;
    QImage *displayImage_;
    QGraphicsPixmapItem *pixmapItem_;
    QGraphicsScene *scene_;
};

#endif // IMAGEDISPLAYWIDGET_H
