#ifndef DRILL_H
#define DRILL_H
#include <QDateTime>
#include <QString>

class Drill
{
public:
    int id;
    QString make;
    QString model;
    QString imagePath;
    QDateTime createdAt;
    QDateTime updatedAt;
};

#endif // DRILL_H
