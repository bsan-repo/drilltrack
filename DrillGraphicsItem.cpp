#include "DrillGraphicsItem.h"



DrillGraphicsItem::DrillGraphicsItem()
{
    QString drillImagePath = ":/images/images/drill_mast_up_small.png";
    equipmentImage_ = new QImage(drillImagePath);
    drillState_ = 0;
}

QRectF DrillGraphicsItem::boundingRect() const
{
    return QRectF(0,0,equipmentImage_->width(),equipmentImage_->height());
    //return QRectF(0,0,500,500);
}

void DrillGraphicsItem::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
{
    QString drillImagePath = ":/images/images/drill_mast_up_small.png";

    switch (drillState_)
    {
    //idle
    case 0:
        drillImagePath = ":/images/images/drill_mast_up_small.png";
        break;

    //moving
    case 1:
        drillImagePath = ":/images/images/drill_mast_down_small.png";
        break;
    }

    equipmentImage_ = new QImage(drillImagePath);

    painter->drawImage(0,0,*equipmentImage_);
}

void DrillGraphicsItem::setDrillState(int drillState)
{
    drillState_ = drillState;
    this->update();
}
