#include "MetricsWidget.h"
#include "ui_MetricsWidget.h"

#include <QDebug>
#include <QList>
#include <QStandardItem>

MetricsWidget::MetricsWidget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::MetricsWidget)
{
    ui->setupUi(this);

    drillId_ = 0;
    make_ = "default_make";
    model_ = "default_model";
    currentHoleId_ = 0;
    holeLocationLat_ = 0;
    holeLocationLon_ = 0;
    holeLocationLat_ = 0;
    holeLocationLon_ = 0;
    locationError_ = 0;

    initializeTable();
}

MetricsWidget::~MetricsWidget()
{
    delete ui;
}

void MetricsWidget::initializeTable()
{
    tableModel_= new QStandardItemModel;

    tableModel_->setHorizontalHeaderItem(0, new QStandardItem(tr("Parameter")));
    tableModel_->setHorizontalHeaderItem(1, new QStandardItem(tr("Value")));

    QList<QStandardItem*> drillIdRow;
    QStandardItem *drillIdItemName = new QStandardItem("Drill ID");
    QStandardItem *drillIdItemValue = new QStandardItem(QString::number(drillId_));
    drillIdRow.append(drillIdItemName);
    drillIdRow.append(drillIdItemValue);
    tableModel_->appendRow(drillIdRow);

    QList<QStandardItem*> makeRow;
    QStandardItem *makeItemName = new QStandardItem("Make");
    QStandardItem *makeItemValue = new QStandardItem(make_);
    makeRow.append(makeItemName);
    makeRow.append(makeItemValue);
    tableModel_->appendRow(makeRow);

    QList<QStandardItem*> modelRow;
    QStandardItem *modelItemName = new QStandardItem("Model");
    QStandardItem *modelItemValue = new QStandardItem(model_);
    modelRow.append(modelItemName);
    modelRow.append(modelItemValue);
    tableModel_->appendRow(modelRow);

    QList<QStandardItem*> currentHoleIdRow;
    QStandardItem *currentHoleIdItemName = new QStandardItem("Current Hole ID");
    QStandardItem *currentHoleIdItemValue = new QStandardItem(QString::number(currentHoleId_));
    currentHoleIdRow.append(currentHoleIdItemName);
    currentHoleIdRow.append(currentHoleIdItemValue);
    tableModel_->appendRow(currentHoleIdRow);

    QList<QStandardItem*> holeLocationRow;
    QStandardItem *holeLocationItemName = new QStandardItem("Hole Location (Lat,Lon)");
    QString holeLocationString = QString::number(holeLocationLat_) + ", " + QString::number(holeLocationLon_);
    QStandardItem *holeLocationItemValue = new QStandardItem(holeLocationString);
    holeLocationRow.append(holeLocationItemName);
    holeLocationRow.append(holeLocationItemValue);
    tableModel_->appendRow(holeLocationRow);

    QList<QStandardItem*> currentLocationRow;
    QStandardItem *currentLocationItemName = new QStandardItem("current Location (Lat,Lon)");
    QString currentLocationString = QString::number(currentLocationLat_) + ", " + QString::number(currentLocationLon_);
    QStandardItem *currentLocationItemValue = new QStandardItem(currentLocationString);
    currentLocationRow.append(currentLocationItemName);
    currentLocationRow.append(currentLocationItemValue);
    tableModel_->appendRow(currentLocationRow);

    QList<QStandardItem*> locationErrorRow;
    QStandardItem *locationErrorItemName = new QStandardItem("Location Error");
    QStandardItem *locationErrorItemValue = new QStandardItem(QString::number(locationError_));
    locationErrorRow.append(locationErrorItemName);
    locationErrorRow.append(locationErrorItemValue);
    tableModel_->appendRow(locationErrorRow);

    ui->tableView->setModel(tableModel_);
    ui->tableView->horizontalHeader()->setSectionResizeMode(QHeaderView::Stretch);
}

void MetricsWidget::refreshTable()
{
    qDebug() << "refreshTable";
    tableModel_->setData(tableModel_->item(METRICS_ROW_DRILL_ID,1)->index(),QString::number(drillId_));
    tableModel_->setData(tableModel_->item(METRICS_ROW_MAKE,1)->index(),make_);
    tableModel_->setData(tableModel_->item(METRICS_ROW_MODEL,1)->index(),model_);
    tableModel_->setData(tableModel_->item(METRICS_ROW_CURRENT_HOLE_ID,1)->index(),QString::number(currentHoleId_));
    QString holeLocationString = QString::number(holeLocationLat_) + ", " + QString::number(holeLocationLon_);
    tableModel_->setData(tableModel_->item(METRICS_ROW_HOLE_LOCATION,1)->index(),holeLocationString);
    QString currentLocationString = QString::number(currentLocationLat_) + ", " + QString::number(currentLocationLon_);
    tableModel_->setData(tableModel_->item(METRICS_ROW_CURRENT_LOCATION,1)->index(),currentLocationString);

    computerError();
    tableModel_->setData(tableModel_->item(METRICS_ROW_LOCATION_ERROR,1)->index(),QString::number(locationError_));
}

void MetricsWidget::computerError()
{

}
