#ifndef METRICSWIDGET_H
#define METRICSWIDGET_H

#include <QWidget>
#include <QStandardItemModel>

namespace Ui {
class MetricsWidget;
}

enum METRICS_ROW {
    METRICS_ROW_DRILL_ID,
    METRICS_ROW_MAKE,
    METRICS_ROW_MODEL,
    METRICS_ROW_CURRENT_HOLE_ID,
    METRICS_ROW_HOLE_LOCATION,
    METRICS_ROW_CURRENT_LOCATION,
    METRICS_ROW_LOCATION_ERROR
};

class MetricsWidget : public QWidget
{
    Q_OBJECT

public:
    explicit MetricsWidget(QWidget *parent = 0);
    ~MetricsWidget();

    void initializeTable();

    void setDrillId(int drillId) {drillId_ = drillId;}

    void setMake(QString make) {make_ = make;}

    void setModel(QString model) {model_ = model;}

    void setCurrentHoleId(int currentHoleId) {currentHoleId_ = currentHoleId;}

    void setHoleLocationLat(double holeLocationLat) {holeLocationLat_ = holeLocationLat;}

    void setHoleLocationLon(double holeLocationLon) {holeLocationLon_ = holeLocationLon;}

    void setCurrentLocationLat(double currentLocationLat) {currentLocationLat_ = currentLocationLat;}

    void setCurrentLocationLon(double currentLocationLon) {currentLocationLon_ = currentLocationLon;}

    void refreshTable();

private:
    Ui::MetricsWidget *ui;

    QStandardItemModel *tableModel_;

    void computerError();

    int drillId_;
    QString make_;
    QString model_;
    int currentHoleId_;
    double holeLocationLat_;
    double holeLocationLon_;
    double currentLocationLat_;
    double currentLocationLon_;
    double locationError_;
};

#endif // METRICSWIDGET_H
