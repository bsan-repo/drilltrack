#-------------------------------------------------
#
# Project created by QtCreator 2015-05-15T20:49:42
#
#-------------------------------------------------

QT       += core gui webkitwidgets

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = DrillTrack
TEMPLATE = app


SOURCES += main.cpp\
        MainWindow.cpp \
    MapWidget.cpp \
    MetricsWidget.cpp \
    ActivityReportWidget.cpp \
    StateRepresentationWidget.cpp \
    DrillGraphicsItem.cpp \
    ImageDisplayWidget.cpp \
    LogNoteWidget.cpp

HEADERS  += MainWindow.h \
    MapWidget.h \
    MetricsWidget.h \
    ActivityReportWidget.h \
    StateRepresentationWidget.h \
    DrillGraphicsItem.h \
    ImageDisplayWidget.h \
    LogNoteWidget.h \
    google_html.h

FORMS    += MainWindow.ui \
    MapWidget.ui \
    MetricsWidget.ui \
    ActivityReportWidget.ui \
    StateRepresentationWidget.ui \
    ImageDisplayWidget.ui \
    LogNoteWidget.ui

RESOURCES += \
    mapresources.qrc
