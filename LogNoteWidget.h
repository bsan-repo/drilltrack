#ifndef LOGNOTEWIDGET_H
#define LOGNOTEWIDGET_H

#include <QWidget>

namespace Ui {
class LogNoteWidget;
}

class LogNoteWidget : public QWidget
{
    Q_OBJECT

public:
    explicit LogNoteWidget(QWidget *parent = 0);
    ~LogNoteWidget();

private:
    Ui::LogNoteWidget *ui;
};

#endif // LOGNOTEWIDGET_H
