#ifndef ACTIVITYREPORTWIDGET_H
#define ACTIVITYREPORTWIDGET_H

#include <QWidget>

namespace Ui {
class ActivityReportWidget;
}

class ActivityReportWidget : public QWidget
{
    Q_OBJECT

public:
    explicit ActivityReportWidget(QWidget *parent = 0);
    ~ActivityReportWidget();

private:
    Ui::ActivityReportWidget *ui;
};

#endif // ACTIVITYREPORTWIDGET_H
