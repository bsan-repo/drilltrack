#include "MainWindow.h"
#include "ui_MainWindow.h"

#include <QDebug>
#include <QTimer>
#include <QFile>

#include "google_html.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    readActivityTestData();
    readPlanTestData();
    readDrillTestData();

    initializeDrillComboBox();

    initializeGlobalMap();
    refreshLocalMap();

    ui->imageDisplayWidget->displayImage(":/images/images/sandvik_DE810.jpeg");

    testDataCount_ = 0;

    int timerPeriod = 1000;

    QTimer *timer = new QTimer(this);
    connect(timer, SIGNAL(timeout()), this, SLOT(updateTimeout()));

    connect(ui->comboBox, SIGNAL(currentIndexChanged(int)), this, SLOT(updateDrillView(int)));


    timer->start(timerPeriod);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::updateTimeout()
{
    qDebug() << "Update timeout";

    //get values from database, currently using mock data
    int selectedDrillId = ui->comboBox->currentData().toInt();
    QString make = drillDataTable_[selectedDrillId-1][1];
    QString model = drillDataTable_[selectedDrillId-1][2];
    QString imagePath = drillDataTable_[selectedDrillId-1][3];
    int currentHoleId = planDataTable_[0][0].toInt();
    double currentLocationLat = activityDataTable_[testDataCount_][2].toDouble();
    double currentLocationLon = activityDataTable_[testDataCount_][3].toDouble();
    e_DrillState currentDrillState = (e_DrillState) activityDataTable_[testDataCount_][4].toInt();

    double holeLocationLat = planDataTable_[0][1].toDouble();
    double holeLocationLon = planDataTable_[0][2].toDouble();

    //update display image
    ui->imageDisplayWidget->displayImage(imagePath);

    //update metricsWidget
    ui->metricsWidget->setDrillId(selectedDrillId);
    ui->metricsWidget->setMake(make);
    ui->metricsWidget->setModel(model);
    ui->metricsWidget->setCurrentHoleId(currentHoleId);
    ui->metricsWidget->setCurrentLocationLat(currentLocationLat);
    ui->metricsWidget->setCurrentLocationLon(currentLocationLon);
    ui->metricsWidget->setHoleLocationLat(holeLocationLat);
    ui->metricsWidget->setHoleLocationLon(holeLocationLon);
    ui->metricsWidget->refreshTable();

    //update stateRepresentationWidget
    ui->stateRepresentationWidget->setDrillState(currentDrillState);
    ui->stateRepresentationWidget->refreshGraphicsView();

    testDataCount_++;
    if (testDataCount_ == 100)
    {
        testDataCount_ = 0;
    }
}

void MainWindow::updateDrillView(int indexId)
{
    refreshLocalMap();
}

void MainWindow::initializeDrillComboBox()
{
    foreach (QStringList stringList, drillDataTable_)
    {
        QString make = stringList[1];
        QString model = stringList[2];
        int drillId = stringList[0].toInt();

        QString displayName = "Drill " + QString::number(drillId) + " - [" + make + ":" + model + "]";
        ui->comboBox->addItem(displayName,QVariant(drillId));
    }
}

void MainWindow::initializeGlobalMap()
{
    // start html
    google_html html;

    int zoom = 6;
    html.generate_begin(zoom);

    // place holes
    int holeCount = 1;
    foreach (QStringList stringList, planDataTable_)
    {
        int currentHoleId = stringList[0].toInt();
        double holeLocationLat = stringList[1].toDouble();
        double holeLocationLon = stringList[2].toDouble();

        QString holeDisplayName = "Hole ID: " + QString::number(currentHoleId);
        QString holeDesignationString = "Hole" + QString::number(currentHoleId);

        html.generate_marker(holeDesignationString.toUtf8().constData(),holeDisplayName.toUtf8().constData(),holeLocationLat,holeLocationLon,3);
    }
//    html.generate_marker("name1","HoverText Marker 1",-22.8,148.6, 0);
//    html.generate_marker("name1","HoverText Marker 2",-22.8,148.6, 1);
//    html.generate_marker("name2","HoverText Marker 3",-22.9,148.3, 2);
//    html.generate_marker("name3","",-22.7,148.4, 3);
//    html.generate_marker("name3","",-22.7,148.5, 4);
//    html.generate_marker("name3","",-22.7,148.6, 5);
//    html.generate_marker("name3","",-22.7,148.7, 6);

    // place rigs
    int drillCount = 1;
    foreach (QStringList stringList, drillDataTable_)
    {
        QString make = stringList[1];
        QString model = stringList[2];
        int drillId = stringList[0].toInt();

        QString displayName = "Drill " + QString::number(drillId) + " - " + make + ":" + model;
        QString designationString = "Drill" + QString::number(drillId);

        QString tempLon = "148." + QString::number(drillId);

        html.generate_rig(designationString.toUtf8().constData(),displayName.toUtf8().constData(),-22.6,tempLon.toDouble(), drillCount);
        drillCount++;
    }
//    html.generate_rig("name3","HoverText Rig 1",-22.6,148.2, 1);
//    html.generate_rig("name3","",-22.6,148.3, 2);
//    html.generate_rig("name3","",-22.6,148.4, 3);
//    html.generate_rig("name3","",-22.6,148.5, 4);
//    html.generate_rig("name3","",-22.6,148.6, 5);
//    html.generate_rig("name3","",-22.6,148.7, 6);

    // draw line
//    std::list<double> lat1;
//    std::list<double> lon1;
//    lat1.push_back(-22.7);
//    lat1.push_back(-22.8);
//    lat1.push_back(-22.9);
//    lon1.push_back(148.5);
//    lon1.push_back(148.8);
//    lon1.push_back(148.6);
//    html.generate_polyline("line", lat1,lon1, 3, 4);

    // draw area
//    std::list<double> lat2;
//    std::list<double> lon2;
//    lat2.push_back(-23.1);
//    lat2.push_back(-23.2);
//    lat2.push_back(-23.3);
//    lon2.push_back(148.5);
//    lon2.push_back(148.8);
//    lon2.push_back(148.6);
//    html.generate_polygon("area", lat2,lon2, 3, 4);

    // finish html
    std::string html_string = html.generate_end();

    QString mapString = QString::fromUtf8(html_string.c_str());

    qDebug() << mapString;

    ui->globalMapWidget->showMap(mapString);
}

void MainWindow::refreshLocalMap()
{
    // start html
    google_html html;

    // place holes
    int currentHoleId = planDataTable_[0][0].toInt();
    double holeLocationLat = planDataTable_[0][1].toDouble();
    double holeLocationLon = planDataTable_[0][2].toDouble();
    double currentLocationLat = activityDataTable_[testDataCount_][2].toDouble();
    double currentLocationLon = activityDataTable_[testDataCount_][3].toDouble();

    QString holeDisplayName = "Hole ID: " + QString::number(currentHoleId);
    QString holeDesignationString = "Hole" + QString::number(currentHoleId);

    int zoom = 7;
    html.generate_begin(zoom,currentLocationLat,currentLocationLon);

    //create marker for hole
    html.generate_marker(holeDesignationString.toUtf8().constData(),holeDisplayName.toUtf8().constData(),holeLocationLat,holeLocationLon, 1);

    // place rig
    int selectedDrillId = ui->comboBox->currentData().toInt();
    QString make = drillDataTable_[selectedDrillId-1][1];
    QString model = drillDataTable_[selectedDrillId-1][2];


    QString displayName = "Drill " + QString::number(selectedDrillId) + " - " + make + ":" + model;
    QString designationString = "Drill" + QString::number(selectedDrillId);

    html.generate_rig(designationString.toUtf8().constData(),displayName.toUtf8().constData(),currentLocationLat,currentLocationLon, 1);

    // finish html
    std::string html_string = html.generate_end();

    QString mapString = QString::fromUtf8(html_string.c_str());

    qDebug() << mapString;

    ui->localMapWidget->showMap(mapString);
}

void MainWindow::readActivityTestData()
{
    QFile file(":/data/testdata/seconds.csv");
    if (!file.open(QIODevice::ReadOnly)) {
        qDebug() << file.errorString();
    }

    int lineCount = 0;
    //while (!file.atEnd()) {
    while (lineCount < 100) {
        QString line = file.readLine();
        QStringList activityDataRow = line.split(',');
        activityDataTable_.append(activityDataRow);
        lineCount++;
    }
}

void MainWindow::readPlanTestData()
{
    QFile file(":/data/testdata/plan.csv");
    if (!file.open(QIODevice::ReadOnly)) {
        qDebug() << file.errorString();
    }

    int lineCount = 0;
    //while (!file.atEnd()) {
    while (lineCount < 100) {
        QString line = file.readLine();
        QStringList dataRow = line.split(',');
        planDataTable_.append(dataRow);
        lineCount++;
    }
}

void MainWindow::readDrillTestData()
{
    QFile file(":/data/testdata/drills.csv");
    if (!file.open(QIODevice::ReadOnly)) {
        qDebug() << file.errorString();
    }

    while (!file.atEnd()) {
        QString line = file.readLine();
        QStringList dataRow = line.split(',');
        drillDataTable_.append(dataRow);
    }
}
