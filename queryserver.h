#ifndef QUERYSERVER_H
#define QUERYSERVER_H

#include <QEventLoop>
#include <QNetworkAccessManager>
#include <QNetworkRequest>
#include <QNetworkReply>
#include <QJsonDocument>
#include <QDebug>

class QueryServer
{
private:
    QNetworkReply *reply;
    QNetworkRequest *req;
public:
    QueryServer();
    QJsonDocument sendRequest(QString url);
};

#endif // QUERYSERVER_H
