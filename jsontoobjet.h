#ifndef JSONTOOBJET_H
#define JSONTOOBJET_H

#include <QJsonDocument>
#include <QJsonArray>
#include <QJsonObject>
#include <QString>
#include <QStringList>
#include <QList>
#include <QDebug>
#include "drill.h"
#include "drillplan.h"
#include "drillactivity.h"

class JsonToObjet
{
public:
    JsonToObjet();

    QList<Drill> JsonToDrill(QJsonDocument &jsonDocument);
    QList<DrillPlan> JsonToDrillPlan(QJsonDocument &jsonDocument);
    QList<DrillActivity> JsonToDrillActivity(QJsonDocument &jsonDocument);
};

#endif // JSONTOOBJET_H
