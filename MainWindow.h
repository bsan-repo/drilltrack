#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private slots:
    void updateTimeout();
    void updateDrillView(int indexId);

private:
    void initializeDrillComboBox();

    void initializeGlobalMap();
    void refreshLocalMap();

    void readActivityTestData();
    void readPlanTestData();
    void readDrillTestData();


    Ui::MainWindow *ui;

    int testDataCount_;


    QList<QStringList> activityDataTable_;

    QList<QStringList> planDataTable_;

    QList<QStringList> drillDataTable_;
};

#endif // MAINWINDOW_H
