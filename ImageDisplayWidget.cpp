#include "ImageDisplayWidget.h"
#include "ui_ImageDisplayWidget.h"

#include <QDebug>

ImageDisplayWidget::ImageDisplayWidget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::ImageDisplayWidget)
{
    ui->setupUi(this);

    scene_ = new QGraphicsScene(this);
    ui->graphicsView->setScene(scene_);

    //display defaultimage
    QImage unscaledImage(":/images/images/schramm_T200XD.jpg");
    displayImage_ = new QImage(unscaledImage.scaledToHeight(150));

    pixmapItem_ = new QGraphicsPixmapItem( QPixmap::fromImage(*displayImage_));

    scene_->addItem(pixmapItem_);
}

ImageDisplayWidget::~ImageDisplayWidget()
{
    delete ui;
}

void ImageDisplayWidget::displayImage(QString imagePath)
{
//    scene_->removeItem(pixmapItem_);
    QString fullImagePath = ":/images/images/" + imagePath;
    fullImagePath.chop(1);
    qDebug() << fullImagePath;

    QImage unscaledImage(fullImagePath);
    displayImage_ = new QImage(unscaledImage.scaledToHeight(150));

    pixmapItem_->setPixmap(QPixmap::fromImage(*displayImage_));
    pixmapItem_->update();

//    scene_->addItem(pixmapItem_);
}


