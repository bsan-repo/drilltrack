#include "MapWidget.h"
#include "ui_MapWidget.h"

#include <QFile>
#include <QMessageBox>

MapWidget::MapWidget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::MapWidget)
{
    ui->setupUi(this);

    initializeMap();
}

MapWidget::~MapWidget()
{
    delete ui;
}

void MapWidget::initializeMap()
{
    QString htmlString = "";

    QFile file(":/templates/localMap.html");

    if(!file.open(QIODevice::ReadOnly)) {
        QMessageBox::information(0, "error", file.errorString());
    }

    QTextStream in(&file);

    while(!in.atEnd()) {
        QString line = in.readLine();
        htmlString.append(line);
        htmlString.append("\n");
    }

    //qDebug() << htmlString;
    ui->webView->setHtml(htmlString);

    file.close();
}

void MapWidget::showMap(QString mapString)
{
    ui->webView->setHtml(mapString);
}
