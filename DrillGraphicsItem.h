#ifndef DRILLGRAPHICSITEM_H
#define DRILLGRAPHICSITEM_H

#include <QGraphicsItem>
#include <QPainter>

class DrillGraphicsItem : public QGraphicsItem
{
public:
    explicit DrillGraphicsItem();

    QRectF boundingRect() const;

    void paint(QPainter *painter, const QStyleOptionGraphicsItem *option,
               QWidget *widget);

    void setDrillState(int drillState);

private:
    QImage *equipmentImage_;

    int drillState_;
};

#endif // DRILLGRAPHICSITEM_H
