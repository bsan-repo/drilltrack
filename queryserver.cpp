#include "queryserver.h"

QueryServer::QueryServer()
{
}

QJsonDocument QueryServer::sendRequest(QString urlPath){
    QJsonDocument jsonResponse;
    QEventLoop eventLoop;

    QNetworkAccessManager mgr;
    QObject::connect(&mgr, SIGNAL(finished(QNetworkReply*)), &eventLoop, SLOT(quit()));

    req = new QNetworkRequest( QUrl( QString(urlPath) ) );
    reply = mgr.get(*req);
    eventLoop.exec();

    if (reply->error() == QNetworkReply::NoError) {

        QString strReply = (QString)reply->readAll();

        qDebug() << "Response:" << strReply;
        jsonResponse = QJsonDocument::fromJson(strReply.toUtf8());

        delete reply;
    }
    else{
        qDebug() << "Error: " <<reply->errorString();
        delete reply;
    }

    return jsonResponse;
}
