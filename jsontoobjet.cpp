#include "jsontoobjet.h"

JsonToObjet::JsonToObjet()
{
}

QList<Drill> JsonToObjet::JsonToDrill(QJsonDocument &jsonDocument){
    QList<Drill> drills;

    if(jsonDocument.isArray()){
        QJsonArray dataArray = jsonDocument.array();
        foreach(QJsonValue value, dataArray){
            if(value.isObject()){
                QJsonObject obj = value.toObject();
                Drill drill;
                drill.id = obj["id"].toString().toInt();
                drill.imagePath = obj["image_path"].toString();
                drill.make = obj["make"].toString();
                drill.model = obj["model"].toString();
                drill.updatedAt = QDateTime::fromString(obj["updated_at"].toString());
                drill.createdAt = QDateTime::fromString(obj["created_at"].toString());
                drills.append(drill);
            }else{
                qDebug()<<"Value is: "<<value.toString();
            }
        }
    }
    return drills;
}

QList<DrillPlan> JsonToObjet::JsonToDrillPlan(QJsonDocument &jsonDocument){
    QList<DrillPlan> drillPlans;

    if(jsonDocument.isArray()){
        QJsonArray dataArray = jsonDocument.array();
        foreach(QJsonValue value, dataArray){
            if(value.isObject()){
                QJsonObject obj = value.toObject();
                DrillPlan drillPlan;
                drillPlan.id = obj["id"].toString().toInt();
                drillPlan.drillId = obj["drill_id"].toString().toInt();
                drillPlan.lat = obj["lat"].toString().toDouble();
                drillPlan.lon = obj["lon"].toString().toDouble();
                drillPlan.depthDrill = obj["depth_drill"].toString().toDouble();
                drillPlan.holeOrder = obj["hole_order"].toString().toInt();
                drillPlan.statusDrill = obj["status_drill"].toString().toInt();
                drillPlan.targetDate = QDateTime::fromString(obj["target_date"].toString());
                drillPlan.updatedAt = QDateTime::fromString(obj["updated_at"].toString());
                drillPlan.createdAt = QDateTime::fromString(obj["created_at"].toString());
                drillPlans.append(drillPlan);
            }else{
                qDebug()<<"Value is: "<<value.toString();
            }
        }
    }
    return drillPlans;
}

QList<DrillActivity> JsonToObjet::JsonToDrillActivity(QJsonDocument &jsonDocument){
    QList<DrillActivity> drillActivities;

    if(jsonDocument.isArray()){
        QJsonArray dataArray = jsonDocument.array();
        foreach(QJsonValue value, dataArray){
            if(value.isObject()){
                QJsonObject obj = value.toObject();
                DrillActivity drillActivity;
                drillActivity.id = obj["id"].toString().toInt();
                drillActivity.drillId = obj["drill_id"].toString().toInt();
                drillActivity.lat = obj["lat"].toString().toDouble();
                drillActivity.lon = obj["lon"].toString().toDouble();
                drillActivity.statusDrill = obj["model"].toString().toInt();
                drillActivity.timeStamp = QDateTime::fromString(obj["time_stamp"].toString());
                drillActivity.updatedAt = QDateTime::fromString(obj["updated_at"].toString());
                drillActivity.createdAt = QDateTime::fromString(obj["created_at"].toString());
                drillActivities.append(drillActivity);
            }else{
                qDebug()<<"Value is: "<<value.toString();
            }
        }
    }
    return drillActivities;
}

