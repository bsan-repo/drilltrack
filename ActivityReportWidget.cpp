#include "ActivityReportWidget.h"
#include "ui_ActivityReportWidget.h"

ActivityReportWidget::ActivityReportWidget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::ActivityReportWidget)
{
    ui->setupUi(this);
}

ActivityReportWidget::~ActivityReportWidget()
{
    delete ui;
}
