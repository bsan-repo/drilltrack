#ifndef DRILLPLAN_H
#define DRILLPLAN_H

#include <QDateTime>

class DrillPlan
{
public:
    int id;
    double lat;
    double lon;
    double depthDrill;
    QDateTime targetDate;
    int statusDrill;
    int holeOrder;
    int drillId;
    QDateTime createdAt;
    QDateTime updatedAt;
};

#endif // DRILLPLAN_H
