#ifndef STATEREPRESENTATIONWIDGET_H
#define STATEREPRESENTATIONWIDGET_H

#include <QWidget>

#include <QGraphicsScene>

#include "DrillGraphicsItem.h"

namespace Ui {
class StateRepresentationWidget;
}

enum e_DrillState {
    DRILL_STATE_IDLE,
    DRILL_STATE_MOVING,
    DRILL_STATE_ACTIVE,
};

class StateRepresentationWidget : public QWidget
{
    Q_OBJECT

public:
    explicit StateRepresentationWidget(QWidget *parent = 0);
    ~StateRepresentationWidget();

    void initializeGraphicsView();

    void setDrillState(e_DrillState drillState);
    void setStateLabel(QString newStateLabel);
    void refreshGraphicsView();

private:
    Ui::StateRepresentationWidget *ui;

    double mastAngle_;

    QString stateLabel_;
    QGraphicsScene *scene_;
    DrillGraphicsItem *drillTruckItem_;
    DrillGraphicsItem *drillMastItem_;
};

#endif // STATEREPRESENTATIONWIDGET_H
